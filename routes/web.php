<?php

use App\Http\Controllers\HotelController;
use App\Http\Controllers\PendaftaranController;
use App\Models\Booking;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/booking', function(){
    return view('booking');
});
Route::post('/booking/store', [HotelController::class, 'store']);

Route::get('/cek-reservasi', [HotelController::class, 'grafik']);
Route::post('/search-data', [HotelController::class, 'search']);
