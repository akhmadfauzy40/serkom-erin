<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use Illuminate\Http\Request;
use stdClass;

class HotelController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'gender' => 'required',
            'no_identitas' => 'required|min:16',
            'tipe' => 'required',
            'harga' => 'required',
            'tanggal' => 'required',
            'durasi' => 'required',
            'total' => 'required',
        ]);

        $data = new Booking();
        $data->nama = $request->nama;
        $data->gender = $request->gender;
        $data->no_identitas = $request->no_identitas;
        $data->tipe = $request->tipe;
        $data->harga = $request->harga;
        $data->tanggal = $request->tanggal;
        $data->durasi = $request->durasi;
        $data->breakfast = $request->breakfast;
        $data->discount = $request->discount;
        $data->total = $request->total;
        $data->save();

        return redirect('booking')->with('success', 'Data telah berhasil ditambahkan!');
    }

    public function search(Request $request)
    {
        $request->validate([
            'no_identitas' => 'required'
        ]);

        $data = Booking::where('no_identitas', $request->no_identitas)->first();

        $grafik = new stdClass();
        $grafik->standar = Booking::where('tipe', 'Standar')->count();
        $grafik->deluxe = Booking::where('tipe', 'Deluxe')->count();
        $grafik->executive = Booking::where('tipe', 'Executive')->count();

        if ($data) {
            return view('cek', ['data' => $data, 'grafik' => $grafik]);
        } else {
            return view('cek', ['error' => 'Data Tidak di Temukan', 'grafik' => $grafik]);
        }
    }

    public function grafik()
    {
        $grafik = new stdClass();
        $grafik->standar = Booking::where('tipe', 'Standar')->count();
        $grafik->deluxe = Booking::where('tipe', 'Deluxe')->count();
        $grafik->executive = Booking::where('tipe', 'Executive')->count();

        return view('cek', compact('grafik'));
    }
}
