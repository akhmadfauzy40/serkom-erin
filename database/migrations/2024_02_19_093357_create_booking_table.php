<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('booking', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->enum('gender', ['Pria', 'Wanita']);
            $table->unsignedBigInteger('no_identitas');
            $table->enum('tipe', ['Standar', 'Deluxe', 'Executive']);
            $table->integer('harga');
            $table->date('tanggal');
            $table->integer('durasi');
            $table->string('breakfast')->default('');
            $table->integer('discount');
            $table->integer('total');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('booking');
    }
};
