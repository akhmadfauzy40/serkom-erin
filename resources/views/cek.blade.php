@extends('layout/layout')

@section('title')
    CEK RESERVASI
@endsection

@section('nav')
    <ul class="nav">
        <li class="nav-item nav-category">
            <span class="nav-link">MENU</span>
        </li>
        <li class="nav-item menu-items">
            <a class="nav-link" href="/">
                <span class="menu-icon">
                    <i class="mdi mdi-information"></i>
                </span>
                <span class="menu-title">INFORMASI HOTEL</span>
            </a>
        </li>
        <li class="nav-item menu-items">
            <a class="nav-link" href="/booking">
                <span class="menu-icon">
                    <i class="mdi mdi-pencil"></i>
                </span>
                <span class="menu-title">BOOKING KAMAR</span>
            </a>
        </li>
        <li class="nav-item menu-items active">
            <a class="nav-link" href="/cek-booking">
                <span class="menu-icon">
                    <i class="mdi mdi-file-table"></i>
                </span>
                <span class="menu-title">CEK DATA BOOKING</span>
            </a>
        </li>
    </ul>
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 grid-margin">
                <div class="card">
                    <div class="card-body">
                        <center>
                            <h1>CEK RESERVASI HOTEL</h1>
                        </center>
                    </div>
                </div>
            </div>
            <div class="col-md-6 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Petunjuk Melakukan Pengecekan Reservasi</h4>
                        <p>Silahkan masukkan nomor identitas anda di kolom yang di sediakan di bawah ini lalu klik tombol cari data
                            untuk melihat data reservasi anda</p>
                        <form action="/search-data" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="no_identitas">Nomor Identitas</label>
                                <input type="number" class="form-control" id="no_identitas" placeholder="Masukkan Nomor Identitas Anda (KTP)"
                                    name="no_identitas" required>
                            </div>
                            @if (isset($error))
                                <div class="alert alert-danger">
                                    {{ $error }}
                                </div>
                            @endif
                            <button type="submit" class="btn btn-primary mr-2">Cari Data</button>
                        </form>
                        <h4 class="card-title mt-4">Grafik Pemesanan Jenis Kamar</h4>
                        <canvas id="doughnutChart" style="height:250px"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-md-6 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Data Reservasi</h4>
                        <div class="form-group">
                            <label for="nama">Nama</label>
                            <input type="text" class="form-control" id="nama"
                                placeholder="{{ isset($data) ? $data->nama : '' }}" disabled>
                        </div>
                        <div class="form-group">
                            <label for="no_identitas">Nomor Identitas</label>
                            <input type="text" class="form-control" id="no_identitas"
                                placeholder="{{ isset($data) ? $data->no_identitas : '' }}" disabled>
                        </div>
                        <div class="form-group">
                            <label for="gender">Gender</label>
                            <input type="text" class="form-control" id="gender"
                                placeholder="{{ isset($data) ? $data->gender : '' }}" disabled>
                        </div>
                        <div class="form-group">
                            <label for="tipe">Tipe Kamar</label>
                            <input type="text" class="form-control" id="tipe"
                                placeholder="{{ isset($data) ? $data->tipe : '' }}" disabled>
                        </div>
                        <div class="form-group">
                            <label for="durasi">Durasi Menginap</label>
                            <input type="text" class="form-control" id="durasi"
                                placeholder="{{ isset($data) ? $data->durasi : '0' }} Hari" disabled>
                        </div>
                        <div class="form-group">
                            <label for="discount">Discount</label>
                            <input type="text" class="form-control" id="discount"
                                placeholder="{{ isset($data) ? $data->discount : '0' }} %" disabled>
                        </div>
                        <div class="form-group">
                            <label for="durasi">Total Pembayaran</label>
                            <input type="text" class="form-control" id="durasi"
                                placeholder="{{ isset($data) ? $data->total : '' }}" disabled>
                        </div>
                        <input type="hidden" name="standar" value="{{ $grafik->standar }}" id="standar">
                        <input type="hidden" name="deluxe" value="{{ $grafik->deluxe }}" id="deluxe">
                        <input type="hidden" name="executive" value="{{ $grafik->executive }}" id="executive">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        var standar = parseInt(document.getElementById('standar').value);
        var deluxe = parseInt(document.getElementById('deluxe').value);
        var executive = parseInt(document.getElementById('executive').value);
        var dataChart = {
            datasets: [{
                data: [standar, deluxe, executive],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.5)',
                    'rgba(54, 162, 235, 0.5)',
                    'rgba(255, 206, 86, 0.5)',
                    'rgba(75, 192, 192, 0.5)',
                    'rgba(153, 102, 255, 0.5)',
                    'rgba(255, 159, 64, 0.5)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
            }],

            // These labels appear in the legend and in the tooltips when hovering different arcs
            labels: [
                'Standar Room',
                'Deluxe Room',
                'Executive Room',
            ]
        };
    </script>
    <!-- plugins:js -->
    <script src="{{ asset('assets/vendors/js/vendor.bundle.base.js') }}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src="{{ asset('assets/vendors/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/typeahead.js/typeahead.bundle.min.js') }}"></script>
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="{{ asset('assets/js/off-canvas.js') }}"></script>
    <script src="{{ asset('assets/js/hoverable-collapse.js') }}"></script>
    <script src="{{ asset('assets/js/misc.js') }}"></script>
    <script src="{{ asset('assets/js/settings.js') }}"></script>
    <script src="{{ asset('assets/js/todolist.js') }}"></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src="{{ asset('assets/js/file-upload.js') }}"></script>
    <script src="{{ asset('assets/js/typeahead.js') }}"></script>
    <script src="{{ asset('assets/js/select2.js') }}"></script>
    <!-- End custom js for this page -->
    <!-- Plugin js for this page -->
    <script src="{{ asset('assets/vendors/chart.js/Chart.min.js') }}"></script>
    <!-- End plugin js for this page -->
    <!-- Custom js for this page -->
    <script src="{{ asset('assets/js/chart.js') }}"></script>
    <!-- End custom js for this page -->
@endsection
