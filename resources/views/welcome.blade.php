@extends('layout/layout')

@section('title')
    INFORMASI HOTEL
@endsection

@section('nav')
    <ul class="nav">
        <li class="nav-item nav-category">
            <span class="nav-link">MENU</span>
        </li>
        <li class="nav-item menu-items active">
            <a class="nav-link" href="/">
                <span class="menu-icon">
                    <i class="mdi mdi-information"></i>
                </span>
                <span class="menu-title">INFORMASI HOTEL</span>
            </a>
        </li>
        <li class="nav-item menu-items">
            <a class="nav-link" href="/booking">
                <span class="menu-icon">
                    <i class="mdi mdi-pencil"></i>
                </span>
                <span class="menu-title">BOOKING KAMAR</span>
            </a>
        </li>
        <li class="nav-item menu-items">
            <a class="nav-link" href="/cek-reservasi">
                <span class="menu-icon">
                    <i class="mdi mdi-file-table"></i>
                </span>
                <span class="menu-title">CEK DATA BOOKING</span>
            </a>
        </li>
    </ul>
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">INFORMASI JENIS DAN FASILITAS KAMAR</h4>

                    <div class="row">
                        <!-- Standar Room -->
                        <div class="col-md-4">
                            <div class="card">
                                <img class="card-img-top"
                                    src="https://imageresizer.arch.software/astoninternational/v2/Images/Kuta/Gallery/Astonkuta_deluxe_room01.jpg?d=1600x1066&location=center"
                                    alt="Standar Room Image">
                                <div class="card-body">
                                    <h5 class="card-title">Standar Room</h5>
                                    <p class="card-text">Kamar Standar menawarkan kenyamanan sederhana dengan AC, TV, dan
                                        fasilitas dasar lainnya. Cocok untuk penginapan yang praktis dan nyaman.</p>
                                </div>
                            </div>
                        </div>

                        <!-- Deluxe Room -->
                        <div class="col-md-4">
                            <div class="card">
                                <img class="card-img-top"
                                    src="https://imageresizer.arch.software/astoninternational/v2/Images/Kuta/Gallery/Astonkuta_family_room01.jpg?d=1600x1066&location=center"
                                    alt="Deluxe Room Image">
                                <div class="card-body">
                                    <h5 class="card-title">Deluxe Room</h5>
                                    <p class="card-text">Kamar Deluxe memberikan pengalaman istimewa dengan fasilitas
                                        tambahan seperti minibar dan balkon pribadi. Ideal untuk tamu yang mencari
                                        kenyamanan ekstra.</p>
                                </div>
                            </div>
                        </div>

                        <!-- Executive Room -->
                        <div class="col-md-4">
                            <div class="card">
                                <img class="card-img-top"
                                    src="https://imageresizer.arch.software/astoninternational/v2/Images/Kuta/Gallery/Astonkuta_new_superior_room01.jpg?d=1600x1066&location=center"
                                    alt="Executive Room Image">
                                <div class="card-body">
                                    <h5 class="card-title">Executive Room</h5>
                                    <p class="card-text">Kamar Executive menyajikan kemewahan dengan fasilitas seperti
                                        minibar, balkon pribadi, dan layanan kamar 24 jam. Pilihan terbaik untuk pengalaman
                                        menginap yang luar biasa.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">DAFTAR HARGA</h4>

                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Jenis Kamar</th>
                                <th>Fasilitas</th>
                                <th>Harga per Malam</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Standar Room</td>
                                <td>AC, TV, kamar mandi dalam, WiFi</td>
                                <td>Rp.300.000</td>
                            </tr>
                            <tr>
                                <td>Deluxe Room</td>
                                <td>AC, TV, kamar mandi dalam, WiFi, minibar, balkon</td>
                                <td>Rp.500.000</td>
                            </tr>
                            <tr>
                                <td>Executive Room</td>
                                <td>AC, TV, kamar mandi dalam, WiFi, minibar, balkon, layanan kamar 24 jam</td>
                                <td>Rp.800.000</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="assets/vendors/js/vendor.bundle.base.js"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src="{{ asset('assets/vendors/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/progressbar.js/progressbar.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/jvectormap/jquery-jvectormap.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
    <script src="{{ asset('assets/vendors/owl-carousel-2/owl.carousel.min.js') }}"></script>
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="{{ asset('assets/js/off-canvas.js') }}"></script>
    <script src="{{ asset('assets/js/hoverable-collapse.js') }}"></script>
    <script src="{{ asset('assets/js/misc.js') }}"></script>
    <script src="{{ asset('assets/js/settings.js') }}"></script>
    <script src="{{ asset('assets/js/todolist.js') }}"></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src="{{ asset('assets/js/dashboard.js') }}"></script>
    <!-- End custom js for this page -->
@endsection
