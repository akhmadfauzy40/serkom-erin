@extends('layout/layout')

@section('title')
    RESERVASI
@endsection

@section('nav')
    <ul class="nav">
        <li class="nav-item nav-category">
            <span class="nav-link">MENU</span>
        </li>
        <li class="nav-item menu-items">
            <a class="nav-link" href="/">
                <span class="menu-icon">
                    <i class="mdi mdi-information"></i>
                </span>
                <span class="menu-title">INFORMASI HOTEL</span>
            </a>
        </li>
        <li class="nav-item menu-items active">
            <a class="nav-link" href="/booking">
                <span class="menu-icon">
                    <i class="mdi mdi-pencil"></i>
                </span>
                <span class="menu-title">BOOKING KAMAR</span>
            </a>
        </li>
        <li class="nav-item menu-items">
            <a class="nav-link" href="/cek-reservasi">
                <span class="menu-icon">
                    <i class="mdi mdi-file-table"></i>
                </span>
                <span class="menu-title">CEK DATA BOOKING</span>
            </a>
        </li>
    </ul>
@endsection

@section('content')
    <div class="content-wrapper">

        <div class="row">
            <div class="col-md-12 grid-margin">
                <div class="card">
                    <div class="card-body">
                        <center>
                            <h1>BOOKING HOTEL</h1>
                        </center>
                    </div>
                </div>
            </div>
            <div class="col-md-6 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Form Pendaftaran</h4>
                        <form class="forms-sample" action="/booking/store" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="nama">Nama Pemesan</label>
                                <input type="text" class="form-control @error('nama') is-invalid @enderror"
                                    id="nama" placeholder="Masukkan Nama Anda" name="nama" required>
                                @error('nama')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Jenis Kelamin</label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="gender" id="gender1"
                                                value="Pria" checked> Pria </label>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="gender" id="gender2"
                                                value="Wanita"> Wanita </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="no_identitas">Nomor Identitas</label>
                                <input type="number" class="form-control" id="no_identitas"
                                    placeholder="Masukkan Nomor Identitas Anda (KTP)" name="no_identitas"
                                    oninput="validateInput(this)" required>
                                <div id="no_identitas_error" class="invalid-feedback"></div>
                                @error('no_identitas')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="tipe">Tipe Kamar</label>
                                <select class="form-control @error('tipe') is-invalid @enderror" id="tipe"
                                    name="tipe" required onchange="updateHarga()">
                                    <option selected disabled>Silahkan Pilih Tipe Kamar</option>
                                    <option value="Standar">Standar</option>
                                    <option value="Deluxe">Deluxe</option>
                                    <option value="Executive">Executive</option>
                                </select>
                                @error('tipe')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="harga">Harga Kamar</label>
                                <input type="text" class="form-control @error('harga') is-invalid @enderror"
                                    id="harga" placeholder="Harga akan muncul otomatis" name="harga" readonly
                                    required>
                                @error('harga')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="tanggal">Tanggal Pesan</label>
                                <input type="date" class="form-control @error('tanggal') is-invalid @enderror"
                                    id="tanggal" placeholder="dd/mm/yyyy" name="tanggal" required>
                                @error('tanggal')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="durasi">Durasi Menginap</label>
                                <div class="input-group">
                                    <input type="text" class="form-control @error('durasi') is-invalid @enderror"
                                        id="durasi" placeholder="Silahkan Masukkan Durasi Menginap" name="durasi"
                                        required oninput="validateNumberInput(this)">
                                    <div class="input-group-append">
                                        <span class="input-group-text">Hari</span>
                                    </div>
                                    <div id="durasi_error" class="invalid-feedback"></div>
                                    @error('durasi')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" name="breakfast" value="Ya">
                                        Termasuk Breakfast </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="discount">Discount</label>
                                <div class="input-group">
                                    <input type="number" class="form-control"
                                    id="discount" placeholder="Discount akan muncul otomatis" name="discount" readonly
                                    required>
                                    <div class="input-group-append">
                                        <span class="input-group-text">%</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="total">Total</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Rp.</span>
                                    </div>
                                    <input type="number" class="form-control" id="total"
                                        placeholder="Harga akan terisi otomatis" name="total" required readonly>
                                </div>
                            </div>
                            <button type="button" class="btn btn-primary mr-2" onclick="hitungTotal()">Hitung Total Bayar</button>
                            <button type="submit" class="btn btn-success mr-2">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-6 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Tentang Hotel Aston</h4>
                        <p>Hotel Aston adalah destinasi mewah yang menyajikan pengalaman menginap yang tak terlupakan.
                            Terletak di lokasi strategis, hotel ini menawarkan kenyamanan dan fasilitas terbaik bagi para
                            tamu.</p>
                        <p>Dengan pemandangan yang memukau dan desain interior yang elegan, Hotel Aston menjadi pilihan
                            utama bagi wisatawan dan pelancong bisnis. Fasilitas modern, layanan ramah, dan kualitas tinggi
                            membuat pengalaman menginap di sini istimewa.</p>
                        <p>Nikmati kenyamanan kelas dunia, keindahan arsitektur, dan kehangatan pelayanan tim profesional
                            Hotel Aston. Apakah Anda dalam perjalanan bisnis atau bersantai, Hotel Aston memberikan
                            pengalaman menginap yang tak tertandingi.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- Script Untuk Generate IPK Otomatis --}}
    <script>
        function updateHarga() {
            var tipeKamar = document.getElementById('tipe').value;
            var hargaInput = document.getElementById('harga');

            switch (tipeKamar) {
                case 'Standar':
                    hargaInput.value = '300000';
                    break;
                case 'Deluxe':
                    hargaInput.value = '500000';
                    break;
                case 'Executive':
                    hargaInput.value = '800000';
                    break;
                default:
                    hargaInput.value = '';
                    break;
            }
        }

        function validateNumberInput(input) {
            var errorDiv = document.getElementById('durasi_error');

            // Memeriksa apakah input mengandung huruf
            if (/[^0-9]/.test(input.value)) {
                errorDiv.innerText = 'Hanya input angka yang diperbolehkan.';
                input.classList.add('is-invalid');
            } else {
                errorDiv.innerText = '';
                input.classList.remove('is-invalid');
            }
        }

        function validateInput(input) {
            var errorDiv = document.getElementById('no_identitas_error');

            if (input.value.length < 16) {
                errorDiv.innerText = 'Minimal 16 karakter.';
                input.classList.add('is-invalid');
            } else {
                errorDiv.innerText = '';
                input.classList.remove('is-invalid');
            }
        }

        function hitungTotal() {
            var hargaKamar = 0;
            var totalInput = document.getElementById('total');
            var checkbox = document.getElementsByName('breakfast')[0];
            var durasi = document.getElementById('durasi');
            var discount = document.getElementById('discount');

            // Mendapatkan harga kamar
            var tipeKamar = document.getElementById('tipe').value;
            switch (tipeKamar) {
                case 'Standar':
                    hargaKamar = 300000;
                    break;
                case 'Deluxe':
                    hargaKamar = 500000;
                    break;
                case 'Executive':
                    hargaKamar = 800000;
                    break;
                default:
                    hargaKamar = 0;
                    break;
            }

            // Menambahkan harga breakfast jika checkbox tercentang
            if (checkbox.checked) {
                hargaKamar += 80000; // Harga breakfast
            }

            if(durasi.value > 2){
                hargaKamar = hargaKamar * durasi.value;
                discount.value = 10;
                var diskon = hargaKamar * 10 / 100;
                hargaKamar -= diskon;
            }else{
                hargaKamar = hargaKamar * durasi.value;
                discount.value = 0;
            }

            // Menetapkan nilai total kembali ke input
            totalInput.value = hargaKamar;
        }
    </script>
    {{-- end Script --}}
    <!-- plugins:js -->
    <script src="{{ asset('assets/vendors/js/vendor.bundle.base.js') }}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src="{{ asset('assets/vendors/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/typeahead.js/typeahead.bundle.min.js') }}"></script>
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="{{ asset('assets/js/off-canvas.js') }}"></script>
    <script src="{{ asset('assets/js/hoverable-collapse.js') }}"></script>
    <script src="{{ asset('assets/js/misc.js') }}"></script>
    <script src="{{ asset('assets/js/settings.js') }}"></script>
    <script src="{{ asset('assets/js/todolist.js') }}"></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src="{{ asset('assets/js/file-upload.js') }}"></script>
    <script src="{{ asset('assets/js/typeahead.js') }}"></script>
    <script src="{{ asset('assets/js/select2.js') }}"></script>
    <!-- End custom js for this page -->
@endsection
